package it.coaching.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import it.coaching.springboot.coach.application.service.RestEndpointService;

@Component
public class AppStartup implements ApplicationListener<ApplicationReadyEvent>{

	
	@Autowired
	private ApplicationContext appContext;
	
//	@Autowired
//	private ApiUtils apiUtils;
	
	@Autowired
	private RestEndpointService repService;

	@Value("${spring.application.name}")
    private String appName;
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		
	    
       /*
		String[] beanNameList = appContext.getBeanNamesForAnnotation(Api.class);
                
        for ( String beanName : beanNameList )
        {
        	Class<?> clazz = appContext.getType(beanName);
        	
        	if ( apiUtils.isAuthorizationEnabled(clazz) )
        	{
        		Method[] methods = clazz.getMethods();
        		
        		for ( Method method : methods )
        		{
        		    
        		    if ( method.getDeclaredAnnotation(ApiOperation.class) == null )
        		    {
        		        continue;
        		    }
        			String apiNickName = apiUtils.getApiNickName(clazz, method);
        			String httpMethod = apiUtils.getHTTPMethod(method);
        			String endPointpath = apiUtils.getRestPointPath(clazz, method);
        			
        			 repService.saveRestEndpoint(RestEndPointDomain.builder()
        			        .restEndpointId("/"+appName+endPointpath)
        			        .apiName(apiUtils.getApiName(clazz))
        			        .apiDescription("["+httpMethod+"]" + apiUtils.getApiName(clazz) +"."+apiUtils.getApiName(clazz))
        			        .javaClass(clazz.getName())
        			        .apiNickname(apiNickName)
        			        .apiPath(endPointpath)
        			        .httpMethod(httpMethod)
        			        .serviceName( appName )
        			        .build() );
        			
        			
        		}
        	}
        	
        	
        }*/
       
	}

}
