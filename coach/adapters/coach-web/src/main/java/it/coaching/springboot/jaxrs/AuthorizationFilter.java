package it.coaching.springboot.jaxrs;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import it.coaching.springboot.coach.application.service.RestEndpointService;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuthorizationFilter implements Filter {

	private final static Logger LOG = LoggerFactory.getLogger(AuthorizationFilter.class);
	
	@Autowired
	private RestEndpointService repService;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		/*
		if ("OPTIONS".equals(req.getMethod())) {
	        res.setStatus(HttpServletResponse.SC_OK);
	        chain.doFilter(request, response); 
	        res.setContentType("application/json");
			LOG.info("Logging Response :{}", res.getContentType());
	        return;
	    }*/
		KeycloakPrincipal principal=null;
		
		if ( SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof KeycloakPrincipal )
		{
			principal = (KeycloakPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        	KeycloakSecurityContext session = principal.getKeycloakSecurityContext();
        	AccessToken accessToken = session.getToken();
        	    		
        	repService.isAuthorized(req.getRequestURI(), accessToken.getRealmAccess().getRoles() );

        	LOG.info("Authorized for request  {} : {}", req.getMethod(), req.getRequestURI());
		}
        
		chain.doFilter(request, response);
		
		LOG.info("Logging Response :{}", res.getContentType());
	}

	

}