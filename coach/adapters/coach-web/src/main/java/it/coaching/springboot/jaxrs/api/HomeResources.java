package it.coaching.springboot.jaxrs.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import it.coaching.springboot.common.annotation.WebAdapter;

@WebAdapter

@RestController
@RequestMapping("/home")
@Tag(name = "home")
public class HomeResources {

	@GetMapping("/home")
	@Operation(summary = "servizio test")
	public String home(
			@RequestParam(required = true) String great) {

		return "Porco Dio "+ great;
		
	}
	

}
