package it.coaching.springboot;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.OAuthFlow;
import io.swagger.v3.oas.annotations.security.OAuthFlows;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

@Configuration
/*@SecurityScheme(
        name = "bearerAuth", // can be set to anything
        type = SecuritySchemeType.OAUTH2,
        scheme = "Bearer",
        flows = 
        bearerFormat = "JWT"
        
)*/

@SecurityScheme(name = "security_auth", type = SecuritySchemeType.OAUTH2,
flows = @OAuthFlows(
		password =  @OAuthFlow(					
					authorizationUrl = "http://localhost:8180/auth/realms/coach_svil/protocol/openid-connect/auth", 
					tokenUrl = "http://localhost:8180/auth/realms/coach_svil/protocol/openid-connect/token"
					)
			)
)


@OpenAPIDefinition(
        info = @Info(title = "Sample API", version = "v1"),
        security = @SecurityRequirement(name = "security_auth") // references the name defined in the line 3 
)
class OpenAPIConfiguration {

}