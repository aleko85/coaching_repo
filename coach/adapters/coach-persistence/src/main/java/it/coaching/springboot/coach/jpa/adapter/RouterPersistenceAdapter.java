package it.coaching.springboot.coach.jpa.adapter;

import java.util.List;

import it.coaching.coach.application.port.out.LoadRestEndPointPort;
import it.coaching.springboot.coach.application.domain.RestEndPointDomain;
import it.coaching.springboot.coach.application.domain.RestEndPointRoleDomain;
import it.coaching.springboot.coach.jpa.entity.RestEndpoint;
import it.coaching.springboot.coach.jpa.mapper.RestEndPointMapper;
import it.coaching.springboot.coach.jpa.mapper.RestEndpointRoleMapper;
import it.coaching.springboot.coach.repository.RestEndpointRepository;
import it.coaching.springboot.coach.repository.enhanced.RestEndpointRoleEnanched;
import it.coaching.springboot.common.annotation.PersistenceAdapter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@PersistenceAdapter
public class RouterPersistenceAdapter implements LoadRestEndPointPort {

	private final RestEndpointRepository restRepo;
	
	private final RestEndpointRoleEnanched ropEnh;
	
	private final RestEndPointMapper restMapper;
	
	private final RestEndpointRoleMapper ropMapper;
	
	
	@Override
	public String saveRestEndpoint(RestEndPointDomain restEndpoint) {
		
		RestEndpoint re = this.restRepo.save(restMapper.mapToEntity(restEndpoint) );
		
		return re.getRestEndpointId();
	}

	@Override
	public RestEndPointRoleDomain loadRestEndPoint(String path, List<String> roles) {
		return ropMapper.mapToDomain(ropEnh.findByRolePath(path+"%", roles));
	}
}


	
