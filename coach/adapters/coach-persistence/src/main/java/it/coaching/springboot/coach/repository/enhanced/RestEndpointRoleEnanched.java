package it.coaching.springboot.coach.repository.enhanced;


import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.coaching.springboot.coach.jpa.entity.RestEndpointRole;



public interface RestEndpointRoleEnanched extends RestEndpointRoleRepository{

	@Cacheable("rest-endpoint-roles")
	@Query("select rop from RestEndpointRole rop where rop.dataFnVal is null and rop.id.restPointId like ?1 and rop.id.roleId in (?2)")
	RestEndpointRole findByRolePath(String restPointId, List<String> roles);
	
	@Query("select rop from RestEndpointRole rop where rop.dataFnVal is null and "
			+ "( :roles is null or rop.id.roleId in (:roles) ) ")
	List<RestEndpointRole> findByRole(@Param("roles")List<String> roles);
}
