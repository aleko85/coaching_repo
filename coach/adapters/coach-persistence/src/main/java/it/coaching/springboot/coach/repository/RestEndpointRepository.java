package it.coaching.springboot.coach.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import it.coaching.springboot.coach.jpa.entity.RestEndpoint;

public interface RestEndpointRepository
extends JpaRepository<RestEndpoint, String>, JpaSpecificationExecutor<RestEndpoint>
{

}
