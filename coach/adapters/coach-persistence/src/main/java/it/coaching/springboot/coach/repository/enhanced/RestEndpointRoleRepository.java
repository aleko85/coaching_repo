package it.coaching.springboot.coach.repository.enhanced;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import it.coaching.springboot.coach.jpa.entity.RestEndpointRole;
import it.coaching.springboot.coach.jpa.entity.RestEndpointRoleId;

public interface RestEndpointRoleRepository
extends JpaRepository<RestEndpointRole, RestEndpointRoleId>, JpaSpecificationExecutor<RestEndpointRole>
{

}

