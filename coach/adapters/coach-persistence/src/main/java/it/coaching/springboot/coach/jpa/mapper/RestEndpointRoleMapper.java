package it.coaching.springboot.coach.jpa.mapper;

import org.springframework.stereotype.Component;

import it.coaching.springboot.coach.application.domain.RestEndPointRoleDomain;
import it.coaching.springboot.coach.jpa.entity.RestEndpointRole;
import it.coaching.springboot.coach.jpa.entity.RestEndpointRoleId;

@Component
public class RestEndpointRoleMapper
{


    public RestEndpointRole mapToEntity(RestEndPointRoleDomain domainRestEndpointRole)
    {
    	if ( domainRestEndpointRole == null )
    	{
    		return null;
    	}
    	
        RestEndpointRole jpaEntity = new RestEndpointRole();

        
        RestEndpointRoleId ropId = new RestEndpointRoleId();
        ropId.setRestPointId(domainRestEndpointRole.getRestEndpointPath());
        ropId.setRoleId(domainRestEndpointRole.getRoleId());
        
		jpaEntity.setId(ropId );

        jpaEntity.setDataInVal(domainRestEndpointRole.getDataInVal());

        jpaEntity.setDataFnVal(domainRestEndpointRole.getDataFnVal());

        return jpaEntity;
    }

    public RestEndPointRoleDomain mapToDomain(RestEndpointRole jpaRestEndpointRole)
    {
    	if ( jpaRestEndpointRole == null )
    	{
    		return null;
    	}
    	
        RestEndPointRoleDomain domainEntity = RestEndPointRoleDomain.builder().build();

        domainEntity.setRestEndpointPath(jpaRestEndpointRole.getId().getRestPointId());
        domainEntity.setRoleId(jpaRestEndpointRole.getId().getRoleId());
                
        domainEntity.setDataInVal(jpaRestEndpointRole.getDataInVal());

        domainEntity.setDataFnVal(jpaRestEndpointRole.getDataFnVal());

        return domainEntity;
    }

}

