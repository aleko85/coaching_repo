package it.coaching.springboot.coach.jpa.mapper;



import org.hibernate.Hibernate;
import org.springframework.stereotype.Component;

import it.coaching.springboot.coach.application.domain.RestEndPointDomain;
import it.coaching.springboot.coach.jpa.entity.RestEndpoint;



/**
 * Home object for domain model class RestEndpoint.
 * 
 * @see it.agriconsultingict.springboot.router.RestEndpoint
 * @author Hibernate Tools
 */
@Component
public class RestEndPointMapper
{

    public RestEndpoint mapToEntity(RestEndPointDomain domainRestEndpoint)
    {
        if (domainRestEndpoint == null)
        {
            return null;
        }

        RestEndpoint jpaEntity = new RestEndpoint();

        jpaEntity.setRestEndpointId(domainRestEndpoint.getRestEndpointId());

        jpaEntity.setApiDescription(domainRestEndpoint.getApiDescription());

        jpaEntity.setApiName(domainRestEndpoint.getApiName());

        jpaEntity.setApiNickname(domainRestEndpoint.getApiNickname());

        jpaEntity.setApiPath(domainRestEndpoint.getApiPath());

        jpaEntity.setServiceName(domainRestEndpoint.getServiceName());

        jpaEntity.setHttpMethod(domainRestEndpoint.getHttpMethod());

        jpaEntity.setJavaClass(domainRestEndpoint.getJavaClass());

        //jpaEntity.setRestEndpointRoles(domainRestEndpoint.getRestEndPointRoles());

        return jpaEntity;
    }

    public RestEndPointDomain mapToDomain(RestEndpoint jpaRestEndpoint)
    {
    	RestEndPointDomain domainEntity = RestEndPointDomain.builder().build();

        if (jpaRestEndpoint == null)
        {
            return null;
        }

        domainEntity.setRestEndpointId(jpaRestEndpoint.getRestEndpointId());

        domainEntity.setApiDescription(jpaRestEndpoint.getApiDescription());

        domainEntity.setApiName(jpaRestEndpoint.getApiName());

        domainEntity.setApiNickname(jpaRestEndpoint.getApiNickname());

        domainEntity.setApiPath(jpaRestEndpoint.getApiPath());

        domainEntity.setServiceName(jpaRestEndpoint.getServiceName());

        domainEntity.setHttpMethod(jpaRestEndpoint.getHttpMethod());

        domainEntity.setJavaClass(jpaRestEndpoint.getJavaClass());

        if (Hibernate.isInitialized(jpaRestEndpoint.getRestEndpointRoles()))
        {
            //domainEntity.setRestEndpointRoles(jpaRestEndpoint.getRestEndpointRoles());
        }

        return domainEntity;
    }

}

