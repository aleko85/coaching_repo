package it.coaching.springboot.coach.jpa.entity;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;


@Entity()
@JsonInclude(value = Include.NON_EMPTY)
@Table(name = "rest_endpoint_role")
public class RestEndpointRole implements java.io.Serializable
{

    @EmbeddedId

    @AttributeOverrides({
            @AttributeOverride(name = "restPointId", column = @Column(name = "rest_point_id", nullable = false, length = 512)),
            @AttributeOverride(name = "roleId", column = @Column(name = "role_id", nullable = false, length = 128)) })
    private RestEndpointRoleId id;

    @NotNull(message = " il campo [restEndpoint] non puo essere nullo ")

    // RestEndpoint
    // org.hibernate.mapping.Property(restEndpoint)

    // @Pattern(regexp="/^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/i{1,512}",
    // message=" [restEndpoint] campo non valido ! ")

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rest_point_id", nullable = false, insertable = false, updatable = false)
    private RestEndpoint restEndpoint;

    // Date
    // org.hibernate.mapping.Property(dataInVal)

    // @Pattern(regexp="/^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/i{0,29}",
    // message=" [dataInVal] campo non valido ! ")

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_in_val", length = 29)
    private Date dataInVal;

    // Date
    // org.hibernate.mapping.Property(dataFnVal)

    // @Pattern(regexp="/^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$/i{0,29}",
    // message=" [dataFnVal] campo non valido ! ")

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_fn_val", length = 29)
    private Date dataFnVal;

    public RestEndpointRole()
    {
    }

    public RestEndpointRole(RestEndpointRoleId id, RestEndpoint restEndpoint)
    {
        this.id = id;
        this.restEndpoint = restEndpoint;
    }

    public RestEndpointRole(RestEndpointRoleId id, RestEndpoint restEndpoint, Date dataInVal, Date dataFnVal)
    {
        this.id = id;
        this.restEndpoint = restEndpoint;
        this.dataInVal = dataInVal;
        this.dataFnVal = dataFnVal;
    }

    public RestEndpointRoleId getId()
    {
        return this.id;
    }

    public void setId(RestEndpointRoleId id)
    {
        this.id = id;
    }

    public RestEndpoint getRestEndpoint()
    {
        return this.restEndpoint;
    }

    public void setRestEndpoint(RestEndpoint restEndpoint)
    {
        this.restEndpoint = restEndpoint;
    }

    public Date getDataInVal()
    {
        return this.dataInVal;
    }

    public void setDataInVal(Date dataInVal)
    {
        this.dataInVal = dataInVal;
    }

    public Date getDataFnVal()
    {
        return this.dataFnVal;
    }

    public void setDataFnVal(Date dataFnVal)
    {
        this.dataFnVal = dataFnVal;
    }

    public boolean equals(Object other)
    {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof RestEndpointRole))
            return false;
        RestEndpointRole castOther = (RestEndpointRole) other;
        if (this.getId() == null && castOther.getId() == null)
        {
            return this.hashCode() == castOther.hashCode();
        }
        if (this.getId() == null && castOther.getId() != null)
        {
            return false;
        }
        if (this.getId() != null && castOther.getId() == null)
        {
            return false;
        }
        return this.getId().equals(castOther.getId());
    }

    // @XmlTransient
    public Object retriveId()
    {
        return this.getId();
    }

    @Override
    public int hashCode()
    {
        if (retriveId() == null)
        {
            return super.hashCode();
        }
        return retriveId().hashCode();
    }

}

