package it.coaching.springboot.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Optional;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import org.springframework.stereotype.Component;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.models.Path;

@Component
public class ApiUtils
{

    public boolean isAuthorizationEnabled(Class<?> clazz)
    {

        Annotation apiAnn = clazz.getAnnotation(Api.class);
        if (apiAnn != null)
        {
            Api api = (Api) apiAnn;
            Authorization[] authorizations = ((Authorization[]) api.authorizations());
            for (Authorization authItem : authorizations)
            {
                if (authItem.value() != null && !authItem.value().isEmpty())
                {
                    return true;
                }
            }
        }
        return false;
    }

    public String getApiNickName(Class clazz, Method method)
    {
        String apiNickName = null;
        ApiOperation apiOp = method.getAnnotation(ApiOperation.class);
        if (apiOp != null)
        {
            ApiOperation api = (ApiOperation) apiOp;
            apiNickName = api.nickname();
        }
        else
        {
            return null;
        }
        return apiNickName;
    }
    
    public String getApiName(Class clazz) {
        Annotation apiAnn = clazz.getDeclaredAnnotation(Api.class);
        if (apiAnn != null) {
            Api api = (Api) apiAnn;
            return api.value();
        }
        return null;
        
    }

    public String getHTTPMethod(Method method)
    {
        Annotation ann = method.getDeclaredAnnotation(POST.class);
        if (ann != null)
        {
            return "POST";
        }
        ann = method.getDeclaredAnnotation(PUT.class);
        if (ann != null)
        {
            return "PUT";
        }
        ann = method.getDeclaredAnnotation(GET.class);
        if (ann != null)
        {
            return "GET";
        }
        ann = method.getDeclaredAnnotation(DELETE.class);
        if (ann != null)
        {
            return "DELETE";
        }
        return "";
    }
    
    public String getRestPointPath(Class clazz, Method method)
    {
        Optional<Annotation> restResourcePath = Optional.ofNullable(clazz.getAnnotation(Path.class));
        /*   
        Optional<Annotation> restEndpointPath = Optional.ofNullable(method.getAnnotationsByType(Path.class ) ) ;
                     
        return restResourcePath
                .map( m -> ((Path) m).value() )
                .orElse("") + 
                restEndpointPath
                .map( m -> ((Path) m).value() )
                .orElse("");*/
        return "";
    }

}

