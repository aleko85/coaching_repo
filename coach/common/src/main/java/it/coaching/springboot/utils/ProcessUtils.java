package it.coaching.springboot.utils;

public class ProcessUtils {

	private ProcessUtils() {};
	
	private static ProcessUtils instance = null ;
	
	public static ProcessUtils getInstance() {
		if ( instance == null )
		{
			instance = new ProcessUtils();
		}
		return instance;
	}
		
	public boolean isWindows() {
	
		return System.getProperty("os.name").toLowerCase().startsWith("windows");
	}
	
	public int executeCommand(String command) throws Exception {
		Process process;
		if (!isWindows()) {
			command=command.replaceAll("\\.cmd", "\\.sh");
			command=command.replaceAll("\\.bat", "\\.sh");
		} else {
			while (command.indexOf('/')>=0) {
			command=command.replace('/', '\\');
			}
			command=command.replace(" \\c ", " /c ");
		}
		process = Runtime.getRuntime().exec(command);
		
		return process.waitFor();
		
		
	}
	
	public String getShell() {
		if (!isWindows()) {
			return"sh ";
		} else {
			return "cmd.exe /c ";
		}
	}
	
	public String getShellCommandEscape() {
		if (isWindows()) {
			return"\"";
		} else {
			return " ";
		}
	}
}
