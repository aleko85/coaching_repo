package it.coaching.springboot.coach.application.service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import it.coaching.coach.application.port.out.LoadRestEndPointPort;
import it.coaching.springboot.coach.application.domain.RestEndPointDomain;
import it.coaching.springboot.coach.application.domain.RestEndPointRoleDomain;
import it.coaching.springboot.coach.application.port.in.RestEnpointUseCase;
import it.coaching.springboot.common.annotation.UseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@UseCase
@Transactional
public class RestEndpointService implements RestEnpointUseCase {

	private final LoadRestEndPointPort loadREP;
	
	@Override
	public String saveRestEndpoint(RestEndPointDomain restEndpoint) {

		return this.loadREP.saveRestEndpoint(restEndpoint);
	}

	@Override
	public boolean isAuthorized(String path, Set<String> roles) {
		
		if ( roles.contains("administrator"))
		{
			return true;
		}
		
		Optional<RestEndPointRoleDomain> rop = Optional.ofNullable(this.loadREP.loadRestEndPoint(path, new ArrayList<String>(roles)));
		
		return rop.map( m -> true).orElseThrow(() -> new AccessDeniedException("Utente non abilitato alla funzionalità"+path) );
		
	}

}
