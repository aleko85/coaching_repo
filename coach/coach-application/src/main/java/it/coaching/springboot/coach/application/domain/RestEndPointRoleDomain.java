package it.coaching.springboot.coach.application.domain;

import java.util.Date;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RestEndPointRoleDomain {
	private String restEndpointPath;
	private String roleId;
	private Date dataInVal;
	private Date dataFnVal;
}
