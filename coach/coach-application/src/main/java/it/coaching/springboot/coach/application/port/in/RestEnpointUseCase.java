package it.coaching.springboot.coach.application.port.in;

import java.util.Set;

import it.coaching.springboot.coach.application.domain.RestEndPointDomain;

public interface RestEnpointUseCase {
	
	public String saveRestEndpoint( RestEndPointDomain restEndpoint );
	public boolean isAuthorized( String path, Set<String> roles);

}
