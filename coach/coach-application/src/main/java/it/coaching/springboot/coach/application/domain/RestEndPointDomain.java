package it.coaching.springboot.coach.application.domain;

import lombok.Builder;
import lombok.Getter;

import lombok.Setter;

@Getter
@Setter
@Builder
public class RestEndPointDomain {
	
	
	
	private String restEndpointId ;
	private String apiNickname;
	private String apiPath ;
	private String serviceName ;
	private String httpMethod ;
	private String javaClass;
	private String apiDescription;
	private String apiName;
}

