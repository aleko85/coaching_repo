package it.coaching.coach.application.port.out;

import java.util.List;

import it.coaching.springboot.coach.application.domain.RestEndPointDomain;
import it.coaching.springboot.coach.application.domain.RestEndPointRoleDomain;

public interface LoadRestEndPointPort {
	public String saveRestEndpoint( RestEndPointDomain restEndpoint );
	public RestEndPointRoleDomain loadRestEndPoint(String path, List<String> roles);

}
